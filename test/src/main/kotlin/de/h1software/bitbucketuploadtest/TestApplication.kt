package de.h1software.bitbucketuploadtest

class TestApplication

private fun version() : String  = TestApplication::class.java.`package`.implementationVersion

fun main(args : Array<String>) {
	println("Version: ${version()}")
}