Overview
========

[Gradle](https://www.gradle.org) plugin that uploads one ore more files to a Bitbucket project's downloads section using the 
[Bitbucket API](https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/downloads#post).

[MIT License](LICENSE) 

Usage Example
=============
See [https://plugins.gradle.org/plugin/de.h1software.bitbucket.upload] for an current version and an alternative way to declare the plugin. 

	import de.h1software.bitbucket.upload.UploadToBitbucket

	plugins {
        id "de.h1software.bitbucket.upload" version "VERSION"
    }

	task dist(type : Zip) {
		from jar.outputs.files

		into ('/libs') {
			from (configurations.runtime)
		}
	}

	task publish(type : UploadToBitbucket) {

		bitbucketProject "someBitBucketUser/someProject"
		username = "someBitbucketUser"
	   
		askForCredentialsIfEmpty = true

		archive {
			name = "foo-${project.version}.zip"
			file = file(dist.archivePath)
		}
	}
	publish.dependsOn(dist)

Password Security
=================
The plugin provides no own means of hiding the required Bitbucket credentials beside a somewhat awkward way to provide the password interactively during build 
(requires task to be configured with _askForCredentialsIfEmpty = true_):

```
task publish(type : UploadToBitbucket) {

	bitbucketProject "someBitBucketUser/someProject"
	username = "someBitbucketUser"
	
	askForCredentialsIfEmpty = true
   
	[...]
}
```
```
> gradle --no-daemon --console=plain publish
[...]
:publish
USERNAME at https://api.bitbucket.org/2.0/repositories/USER/PROJECT/downloads password:
[...]
```
Gradles _--no-daemon_ parameter is required for interactive password (and/or username) entry to work, _--console=plain_ prevents the console output of the password prompt from being messed up 
(See [Gradle Commandline](https://docs.gradle.org/current/userguide/gradle_command_line.html) for descriptions of those parameters)

For non-interactive usecases the only way to keep the password out of the source repository is for it to be given as a variable which is set outside of the projects sources 
(e.g. via _-Pvariable=value_ on the commandline which makes the password visible in process listings, or via Gradles [user properties](https://docs.gradle.org/current/userguide/build_environment.html))  

	task publish(type : UploadToBitbucket) {

		bitbucketProject "someBitBucketUser/someProject"
		username = "someBitbucketUser"
		
		password = "${some_variable_defined_elsewhere}"

		[...]
	}

HTTP Settings
=============
HTTP Settings (proxy server address, credentials for proxy authentication) are taken from the build's [system properties](https://docs.gradle.org/current/userguide/build_environment.html#sec:accessing_the_web_via_a_proxy) 

Overwriting Resources
=====================
The upload task will check whether a resource with the given name already exists on Bitbucket's servers before uploading a file, the task will fail the build if that is the case.
This behaviour can be changed by setting the task property "overwrite" to true:

	task publish(type : UploadToBitbucket) {
		[...]
		overwrite = true
		[...]
	}
Buildscript Classpath
=====================
The artifact _de.h1software.bitbucket:upload-plugin_ depends on _org.apache.httpcomponents:httpclient_ and _org.apache.httpcomponents:httpmime_ which is a thing to keep in mind
when looking for library version conflicts in the [buildscript classpath](https://docs.gradle.org/current/userguide/organizing_build_logic.html#sec:build_script_external_dependencies).  

Task Configuration Reference
============================

* **bitbucketProject** method: USERNAME/PROJECTNAME-combination identifying the Bitbucket project. Alternative ways of identifiying the Bitbucket project 
	are a **bitbucketProject** closure or the **apiDownloadsUrl** property (see below). One of the three is required, they are mutually exclusive. 
	```
	bitbucketProject 'foo/bar'
	```

* **username** property: Username to be used for authentication with Bitbucket's API. If this property is set it's value will be used for authentication even
	if **askForCredentialsIfEmpty** is set to true. Required unless **askForCredentialsIfEmpty** is set to true.
	```
	username = "foo"
	``` 

* **password** property: Password to be used for authentication with Bitbucket's API. If this property is set it's value will be used for authentication even
	if **askForCredentialsIfEmpty** is set to true. It is obviously not advisable to specify the literal value of the password in a project's build.gradle or gradle.properties, 
	see "Password Security". Required unless **askForCredentialsIfEmpty** is set to true.
	```
	password = "${some_variable_defined_elsewhere}"
	```

* **askForCredentialsIfEmpty** property: Setting this to _true_ will cause the task to prompt for **username** and/or **password** on the console (if possible) if those properties are not specified in the tasks configuration. 
	Will fail the build if either **username** or **password** are missing and no console is available, which will be the case if the build
	is running in a CI-environment or within the Gradle daemon (this is the default, see "Password Security" above). 
	Optional, defaults to _false_.
	```
	askForCredentialsIfEmpty = true
	```

* **overwrite** property: Setting this to _true_ will cause the task to _not_ check for the existence of a resource with the same name in the project's downloads section
	on Bitbucket's servers prior to uploading an **archive**. Existing resources will be overwritten in this case. 
	Optional, defaults to _false_.
	```
	overwrite = true
	```
* **archive** closure(s): Each closure specifies a combination of a local file to be uploaded and it's target name in the Bitbucket project's downloads section. 
	Optional, the task will do nothing if no archive is specified.
	```
	archive {
		name = "bar-${project.version}.jar"
		file = file(jar.archivePath)
	}
	archive {
		name = "bar-${project.version}.zip"
		file = file(dist.archivePath)
	}
	```
	* **name** property: Name of the resource in the Bitbucket project's downloads section after upload. Optional, will default to the local file's name.
		```
		name = "foo.zip"
		```
	* **file** property: File object specifying the file to upload. Required.
		```
		file = file("/some/where/over/the/rainbow.zip")
		```

* **apiDownloadsUrl** property: Allows specifying the full URL of the project's download section to upload to.  
	Introduced for local testing; mutually exclusive with **bitbucketProject(String)** method and **bitbucketProject** closure described above and below.
	```
	apiDownloadsUrl = 'https://api.bitbucket.org/2.0/repositories/USER/PROJECT/downloads'
	```

* **bitbucketProject** closure: Allows specifying the base URL used for upload along with the project's name.  
	Introduced for local testing; mutually exclusive with **bitbucketProject(String)** method and **apiDownloadsUrl** property described above.
	```
	bitbucketProject {
		name = "foo/bar
		apiBaseUrl = 'https://api.bitbucket.org/2.0/'
	 }
	 ```
 
	* **name** USERNAME/PROJECTNAME-Combination identifying the Bitbucket project
		```
		name = "foo/bar
		```
	* **apiBaseUrl** Base url of Bitbuckets HTTP API
		```
		apiBaseUrl = 'https://api.bitbucket.org/2.0/'
		```
