package de.h1software.bitbucket.upload

data class BitbucketProjectConfiguration(var name : String? = null,
										 var apiBaseUrl : String = "https://api.bitbucket.org/2.0") {

	internal fun toBitbucketProject() : BitbucketProject = this.name?.nameToBitbucketProject(this.apiBaseUrl) ?: throw IllegalArgumentException("missing name in $this")
}