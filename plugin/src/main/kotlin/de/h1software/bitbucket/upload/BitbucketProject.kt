package de.h1software.bitbucket.upload

import java.net.URL

// https://api.bitbucket.org/2.0/repositories/evzijst/git-tests/downloads
data class BitbucketProject(private val apiProjectUrl : URL) {

	val downloadsUrl : URL get() = this.apiProjectUrl.resolve("downloads")

	internal fun resolveDownloadsUrl(archive : BitbucketArchive) : URL = this.downloadsUrl.resolve(archive.name)

	override fun toString() : String = this.apiProjectUrl.toString()
}

internal fun String.urlToBitbucketProject() : BitbucketProject = BitbucketProject(URL(this))
internal fun String.nameToBitbucketProject(apiBaseUrl : String) : BitbucketProject = "${apiBaseUrl}/repositories/${this}".urlToBitbucketProject()