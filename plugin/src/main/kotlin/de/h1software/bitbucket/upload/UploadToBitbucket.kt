package de.h1software.bitbucket.upload

import org.gradle.api.Action
import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction

// TODO allow archive { file = <String> }
// TODO remove inputs never up-to-date as we do not want to check output, which resides on remote http server
open class UploadToBitbucket : DefaultTask() {

	init {
		this.group = "Publishing"
		this.description = "Uploads one or more files to a Bitbucket project's downloads section"
	}

	var console : InteractiveConsole? = System.console()?.let { SystemConsole(it) }

	@Input
	val archives : MutableList<BitbucketArchiveConfiguration> = ArrayList<BitbucketArchiveConfiguration>()
	@Input
	var bitbucketProject : BitbucketProject? = null
		set(value : BitbucketProject?) {
			if (field!=null) {
				throw IllegalStateException("bitbucketProject already set to $field")
			}
			field = value
		}

	var apiProjectUrl : String?
		get() = this.bitbucketProject?.downloadsUrl?.toString()
		set(value : String?) {
			this.bitbucketProject = value?.urlToBitbucketProject()
		}

	var username : String? = null
	var password : String? = null

	var overwrite : Boolean = false

	@Input
	var askForCredentialsIfEmpty = false

	@Input
	fun archive(action : Action<BitbucketArchiveConfiguration>) {
		val archive : BitbucketArchiveConfiguration = BitbucketArchiveConfiguration().apply { action.execute(this) }
		this.archives += archive
		archive.file?.let { this.inputs.file(it) }
	}

	fun bitbucketProject(configuration : BitbucketProjectConfiguration) {
		this.bitbucketProject = configuration.toBitbucketProject()
	}

	fun bitbucketProject(action : Action<BitbucketProjectConfiguration>) {
		this.bitbucketProject(de.h1software.bitbucket.upload.BitbucketProjectConfiguration().apply { action.execute(this) })
	}

	fun bitbucketProject(name : String) {
		this.bitbucketProject(de.h1software.bitbucket.upload.BitbucketProjectConfiguration(name))
	}

	private val configuredBitbucketProject : BitbucketProject
		get() = this.bitbucketProject ?: throw IllegalStateException("Bitbucket project not configured - please specify either 'bitBucketProject' or 'apiProjectUrl' in task configuration")


	private val bitbucketCredentials : BitbucketCredentials get() {
		val console : InteractiveConsole? = this.console ?: System.console()?.let { SystemConsole(it) }

		val username : String = this.username
				?: if (this.askForCredentialsIfEmpty) { console?.readUsername("\n${this.apiProjectUrl} username:") } else { null }
				?: throw IllegalArgumentException("'username' not specified in task configuration and task is unable to prompt for it.\n" +
				"Please specify the username in the task configuration, do not set askForCredentialsIfEmpty to false and/or run the build in an environment where a console is available (i.e. outside of IDE and ci-build)")
		val password : String = this.password
				?: if (this.askForCredentialsIfEmpty) { console?.readPassword("\n${username} at ${this.apiProjectUrl} password:") } else { null }
				?: throw IllegalArgumentException("'password' not specified in task configuration and task is unable to prompt for it.\n" +
				"Please either specify the password in the task configuration, or do not set askForCredentialsIfEmpty to false (was ${this.askForCredentialsIfEmpty}) " +
				"and/or run the build in an environment where a console is available (was ${console}) (i.e. with --no-daemon outside of IDE and ci-build)")

		return BitbucketCredentials(username, password)
	}


	@TaskAction
	fun uploadFiles() {
		if (this.archives.isNotEmpty()) {

			BitbucketApiClient(this.configuredBitbucketProject, this.bitbucketCredentials, this.logger).use { apiClient : BitbucketApiClient ->

				val archives : List<BitbucketArchive> = this.archives.map { it.toBitbucketArchive() }

				if (!this.overwrite) {
					archives.find {
						this.logger.info("Checking whether ${it} exists in ${this.configuredBitbucketProject}")
						apiClient.existsOnServer(it)
					}?.let { existingArchive : BitbucketArchive ->
						throw GradleException("Resource '${existingArchive.name}' already exists at ${this.configuredBitbucketProject.downloadsUrl}, specify 'overwrite = true' if you want to overwrite existing resources")
					}
				}

				this.logger.info("Uploading archive(s) ${archives} to ${this.configuredBitbucketProject}")

				apiClient.upload(archives)

				this.logger.lifecycle("Sucessfully uploaded archive(s) ${archives} to ${this.configuredBitbucketProject}")
			}
		} else {
			this.logger.lifecycle("No archives specified for upload")
		}
	}
}