package de.h1software.bitbucket.upload

import java.io.Console

class SystemConsole(private val delegate : Console) : InteractiveConsole {

	override fun readUsername(prompt : String) : String = this.delegate.readLine(prompt)

	override fun readPassword(prompt : String) : String = String(this.delegate.readPassword(prompt))
}