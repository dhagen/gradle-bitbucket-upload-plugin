package de.h1software.bitbucket.upload

import java.io.File

class BitbucketArchiveConfiguration {
	var name : String? = null
	var file : File? = null

	internal fun toBitbucketArchive() : BitbucketArchive {
		val file = this.file
		if (file!=null) {
			val name : String = this.name ?: file.name
			return BitbucketArchive(name, file)
		} else {
			throw IllegalArgumentException("required property 'file' not configured in $this")
		}
	}
}