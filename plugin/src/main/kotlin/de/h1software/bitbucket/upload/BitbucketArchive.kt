package de.h1software.bitbucket.upload

import java.io.File

internal data class BitbucketArchive(val name : String, val file : File) {
	// TODO check if setting this helps with bitbucket content disposition on download
	val contentType : String = "application/octet-stream"

	override fun toString() : String = "${this.file} -> ${this.name}"
}