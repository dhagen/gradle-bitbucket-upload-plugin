package de.h1software.bitbucket.upload

import org.apache.http.HttpHost
import org.apache.http.HttpRequest
import org.apache.http.auth.UsernamePasswordCredentials
import org.apache.http.client.AuthCache
import org.apache.http.client.CredentialsProvider
import org.apache.http.client.RedirectStrategy
import org.apache.http.client.methods.CloseableHttpResponse
import org.apache.http.client.methods.HttpHead
import org.apache.http.client.methods.HttpPost
import org.apache.http.client.protocol.HttpClientContext
import org.apache.http.entity.ContentType
import org.apache.http.entity.mime.MultipartEntityBuilder
import org.apache.http.impl.auth.BasicScheme
import org.apache.http.impl.client.BasicAuthCache
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClients
import org.apache.http.impl.client.SystemDefaultCredentialsProvider
import org.gradle.api.logging.Logger
import java.io.Closeable
import java.io.IOException

internal fun HttpHost.createBasicCredentialsProvider(username : String, password : String) : CredentialsProvider = SystemDefaultCredentialsProvider().apply {
	this.setCredentials(this@createBasicCredentialsProvider.authScope, UsernamePasswordCredentials(username, password))
}

internal fun HttpHost.createBasicAuthCache() : AuthCache = BasicAuthCache().apply {
	this.put(this@createBasicAuthCache, BasicScheme())
}

internal fun HttpHost.createPreemptiveBasicAuthClientContext(username : String, password : String) : HttpClientContext =
		HttpClientContext.create().apply {
			this.credentialsProvider = this@createPreemptiveBasicAuthClientContext.createBasicCredentialsProvider(username, password)
			this.authCache = this@createPreemptiveBasicAuthClientContext.createBasicAuthCache()
		}

// TODO http tracing depending on gradle log level
internal class BitbucketApiClient(private val project : BitbucketProject,
								  private val credentials : BitbucketCredentials,
								  private val logger : Logger) : Closeable {

	private val httpClient : CloseableHttpClient by lazy {
		// not fun to test...
		// FIXME disableRedirectHandling is nonsense, but following redirects for HEAD on existing resources currently ends in "403 permission denied"
		HttpClients.custom().useSystemProperties().disableRedirectHandling().build()
	}

	private val httpHost : HttpHost = this.project.downloadsUrl.toHttpHost()

	private val httpContext : HttpClientContext by lazy {
		this.httpHost.createPreemptiveBasicAuthClientContext(this.credentials.username, this.credentials.password)
	}

	override fun close() : Unit {
		if (this::httpClient.isInitialized()) {
			this.httpClient.close()
		}
	}

	private fun executeHttpRequest(request : HttpRequest) : CloseableHttpResponse = this.httpClient.execute(this.httpHost, request, this.httpContext)

	fun upload(archives : Iterable<BitbucketArchive>) {

		val httpPost = HttpPost(this.project.downloadsUrl.toURI())

		val multipartEntityBuilder : MultipartEntityBuilder = MultipartEntityBuilder.create()

		archives.forEach { archive : BitbucketArchive ->
			multipartEntityBuilder.addBinaryBody("files", archive.file, archive.contentType.toContentType(), archive.name)
		}

		httpPost.entity = multipartEntityBuilder.build()

		logger.debug("Uploading ${archives.map { it.file }} to ${httpPost.uri}")

		this.executeHttpRequest(httpPost).use { httpResponse : CloseableHttpResponse ->
			if (!httpResponse.ok) {
				throw IOException("${httpPost.uri} responded with: ${httpResponse.statusLine}")
			} else {
				logger.debug("Successfully uploaded ${archives.map { it.file }} to ${httpPost.uri}")
			}
		}
	}

	fun existsOnServer(archive : BitbucketArchive) : Boolean {
		val httpHead = HttpHead(this.project.resolveDownloadsUrl(archive).toURI())

		logger.debug("Checking whether ${httpHead.uri} exists")

		this.executeHttpRequest(httpHead).use { httpResponse : CloseableHttpResponse ->
			if (httpResponse.ok || httpResponse.moved) { // yup, the "api" does that
				logger.debug("Found ${httpHead.uri}")
				return true
			} else if (httpResponse.notFound) {
				logger.debug("Did not find ${httpHead.uri}")
				return false
			} else {
				throw IOException("${httpHead.uri} responded with: ${httpResponse.statusLine}")
			}
		}
	}
}

fun String.toContentType() : ContentType = ContentType.create(this)