package de.h1software.bitbucket.upload

interface InteractiveConsole {
	fun readUsername(prompt : String) : String
	fun readPassword(prompt : String) : String
}