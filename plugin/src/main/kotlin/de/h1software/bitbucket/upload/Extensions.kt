package de.h1software.bitbucket.upload

import org.apache.http.Header
import org.apache.http.HttpHeaders
import org.apache.http.HttpHost
import org.apache.http.HttpMessage
import org.apache.http.HttpRequest
import org.apache.http.HttpResponse
import org.apache.http.HttpStatus
import org.apache.http.StatusLine
import org.apache.http.auth.AuthScope
import java.net.BindException
import java.net.InetAddress
import java.net.ServerSocket
import java.net.URI
import java.net.URL
import kotlin.reflect.KProperty0
import kotlin.reflect.jvm.isAccessible

fun URL.resolve(path : String) : URL = URL("${this}/$path")

fun <T> KProperty0<T>.isInitialized(): Boolean {
	isAccessible = true
	return (getDelegate() as? Lazy<*>)?.isInitialized() ?: true
}

fun URI.toHttpHost() : HttpHost = HttpHost(this.host, this.port, this.scheme)
fun URL.toHttpHost() : HttpHost = this.toURI().toHttpHost()

val HttpHost.authScope : AuthScope get() = AuthScope(this.hostName, this.port)

fun InetAddress.bindServerSocket(port : Int) : ServerSocket = ServerSocket(port, 0, this)

fun InetAddress.findBindablePort(range : IntRange) : Int =
	range.first { port : Int ->
		try {
			this.bindServerSocket(port).close()
			true
		} catch (e : BindException) {
			false
		}
	}
val StatusLine.ok : Boolean get() = this.statusCode == HttpStatus.SC_OK || this.statusCode == HttpStatus.SC_CREATED || this.statusCode == HttpStatus.SC_NO_CONTENT
val StatusLine.moved : Boolean get() = this.statusCode == HttpStatus.SC_MOVED_PERMANENTLY || this.statusCode == HttpStatus.SC_MOVED_TEMPORARILY
val StatusLine.notFound : Boolean get() = this.statusCode == HttpStatus.SC_NOT_FOUND

val HttpResponse.ok : Boolean get() = this.statusLine.ok
val HttpResponse.moved : Boolean get() = this.statusLine.moved
val HttpResponse.notFound : Boolean get() = this.statusLine.notFound

val HttpMessage.stringDump : String
	get() = this.allHeaders
			.map { header : Header ->
				header.name to if (header.name == HttpHeaders.AUTHORIZATION) {
					"*****"
				} else {
					header.value
				}
			}
			.map { (name, value) -> "${name}: ${value}" }
			.joinToString("\n")

val HttpRequest.stringDump : String
	get() = "${this.requestLine}\n${(this as HttpMessage).stringDump}"

val HttpResponse.stringDump : String
	get() = "${this.statusLine}\n${(this as HttpMessage).stringDump}\n\n${this.entity?.content?.reader()?.readText()}"