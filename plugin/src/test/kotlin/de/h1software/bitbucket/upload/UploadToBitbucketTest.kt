package de.h1software.bitbucket.upload

import de.h1software.bitbucket.upload.test.FakeBitbucketApiServer
import de.h1software.bitbucket.upload.test.MockInteractiveConsole
import org.gradle.api.Action
import org.gradle.api.GradleException
import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder
import java.io.File

fun Project.applyPluginAndAddTask(taskName : String) : Unit {
	this.plugins.apply("de.h1software.bitbucket.upload")
	this.tasks.create(taskName, UploadToBitbucket::class.java).let { task : UploadToBitbucket ->
		task.group = "Bitbucket"
		task.description = "Upload file(s) to a Bitbucket project's downloads section"
	}
}

class UploadToBitbucketTest {

	@get:Rule
	val mockBitbucketApiServer = FakeBitbucketApiServer()

	@get:Rule
	val temporaryFolder = TemporaryFolder()


	@Test
	fun taskFilesCanBeConfigured() {
		// setup: new project
		val project : Project = ProjectBuilder.builder().build()

		// setup: apply plugin: 'de.h1software.bitbucket.upload'
		project.applyPluginAndAddTask("uploadToBitbucket")

		// setup: create files to upload
		val filesToUpload : List<File> = listOf(this.temporaryFolder.newFile(), this.temporaryFolder.newFile())

		// exercise: configure task upload properties
		val task : UploadToBitbucket = project.tasks.getByPath("uploadToBitbucket") as UploadToBitbucket

		task.archive(Action {
			it.name = "lala"
			it.file = filesToUpload[0]
		})
		task.archive(Action {
			it.name = "lulu"
			it.file = filesToUpload[1]
		})

		// check
		Assert.assertEquals(2, task.archives.size)

		Assert.assertEquals("lala", task.archives[0].name)
		Assert.assertEquals(filesToUpload[0], task.archives[0].file)

		Assert.assertEquals("lulu", task.archives[1].name)
		Assert.assertEquals(filesToUpload[1], task.archives[1].file)
	}

	@Test
	fun filesCanBeUploaded() {
		// setup: create project dir
		val projectDir = this.temporaryFolder.newFolder()

		// setup: create files to upload
		val filesToUpload : List<File> = listOf(
				projectDir.resolve("foo.txt").apply { this.writeText("foo") },
				projectDir.resolve("bar.txt").apply { this.writeText("bar") }
		)

		// setup: configure project
		// setup: new project
		val project : Project = ProjectBuilder.builder().build()

		// setup: apply plugin: 'de.h1software.bitbucket.upload'
		project.applyPluginAndAddTask("uploadToBitbucket")

		// exercise: configure task upload properties
		val task : UploadToBitbucket = project.tasks.getByPath("uploadToBitbucket") as UploadToBitbucket
		task.bitbucketProject(Action {
			it.name = "goo/zar"
			it.apiBaseUrl = this.mockBitbucketApiServer.url.toString()
		})
		task.username = "moo"
		task.password = "jar"
		task.console = null
		task.archive(Action {
			it.name = "FOO.TxT"
			it.file = filesToUpload[0]
		})
		task.archive(Action {
			it.name = "BAR.TxT"
			it.file = filesToUpload[1]
		})

		// exercise
		task.uploadFiles()

		// check
		this.mockBitbucketApiServer.assertFileUploaded("goo/zar", "moo", "jar", "FOO.TxT", filesToUpload[0].readBytes())
		this.mockBitbucketApiServer.assertFileUploaded("goo/zar", "moo", "jar", "BAR.TxT", filesToUpload[1].readBytes())
	}

	@Test
	fun archiveResourceNameCanBeInferredFromFilename() {
		// setup: create project dir
		val projectDir = this.temporaryFolder.newFolder()

		// setup: create files to upload
		val filesToUpload : List<File> = listOf(
				projectDir.resolve("foo-43.23.zip").apply { this.writeText("foo") },
				projectDir.resolve("bar-7.txt").apply { this.writeText("bar") }
		)

		// setup: configure project
		// setup: new project
		val project : Project = ProjectBuilder.builder().build()

		// setup: apply plugin: 'de.h1software.bitbucket.upload'
		project.applyPluginAndAddTask("uploadToBitbucket")

		// exercise: configure task upload properties
		val task : UploadToBitbucket = project.tasks.getByPath("uploadToBitbucket") as UploadToBitbucket
		task.bitbucketProject(Action {
			it.name = "goo/zar"
			it.apiBaseUrl = this.mockBitbucketApiServer.url.toString()
		})
		task.username = "moo"
		task.password = "jar"
		task.console = null
		task.archive(Action {
			it.file = filesToUpload[0]
		})
		task.archive(Action {
			it.file = filesToUpload[1]
		})

		// exercise
		task.uploadFiles()

		// check
		this.mockBitbucketApiServer.assertFileUploaded("goo/zar", "moo", "jar", "foo-43.23.zip", filesToUpload[0].readBytes())
		this.mockBitbucketApiServer.assertFileUploaded("goo/zar", "moo", "jar", "bar-7.txt", filesToUpload[1].readBytes())
	}

	@Test
	fun passwordCanBeGivenInteractively() {

		// setup: create project dir
		val projectDir = this.temporaryFolder.newFolder()

		// setup: create files to upload
		val filesToUpload : List<File> = listOf(
				projectDir.resolve("foo.txt").apply { this.writeText("foo") }
		)

		// setup: configure project
		// setup: new project
		val project : Project = ProjectBuilder.builder().build()

		// setup: apply plugin: 'de.h1software.bitbucket.upload'
		project.applyPluginAndAddTask("uploadToBitbucket")

		// exercise: configure task upload properties
		val task : UploadToBitbucket = project.tasks.getByPath("uploadToBitbucket") as UploadToBitbucket

		task.username = "moo"
		task.console = MockInteractiveConsole("shouldnotbeused", "jar")

		task.askForCredentialsIfEmpty = true

		task.bitbucketProject(Action {
			it.name = "goo/zar"
			it.apiBaseUrl = this.mockBitbucketApiServer.url.toString()
		})
		task.archive(Action {
			it.name = "FOO.TxT"
			it.file = filesToUpload[0]
		})

		// exercise
		task.uploadFiles()

		// check
		this.mockBitbucketApiServer.assertFileUploaded("goo/zar", "moo", "jar", "FOO.TxT", filesToUpload[0].readBytes())
	}

	@Test
	fun usernameAndPasswordCanBeGivenInteractively() {
		// setup: create project dir
		val projectDir = this.temporaryFolder.newFolder()

		// setup: create files to upload
		val filesToUpload : List<File> = listOf(
				projectDir.resolve("foo.txt").apply { this.writeText("foo") }
		)

		// setup: configure project
		// setup: new project
		val project : Project = ProjectBuilder.builder().build()

		// setup: apply plugin: 'de.h1software.bitbucket.upload'
		project.applyPluginAndAddTask("uploadToBitbucket")

		// exercise: configure task upload properties
		val task : UploadToBitbucket = project.tasks.getByPath("uploadToBitbucket") as UploadToBitbucket

		task.console = MockInteractiveConsole("moo", "jar")

		task.askForCredentialsIfEmpty = true

		task.bitbucketProject(Action {
			it.name = "goo/zar"
			it.apiBaseUrl = this.mockBitbucketApiServer.url.toString()
		})
		task.archive(Action {
			it.name = "FOO.TxT"
			it.file = filesToUpload[0]
		})

		// exercise
		task.uploadFiles()

		// check
		this.mockBitbucketApiServer.assertFileUploaded("goo/zar", "moo", "jar", "FOO.TxT", filesToUpload[0].readBytes())
	}

	@Test
	fun overwritingAResourceIsRefusedUnlessExplicitlyConfigured() {
		// setup: create project dir
		val projectDir = this.temporaryFolder.newFolder()

		// setup: create files to upload
		val fileToUpload : File = projectDir.resolve("foo.txt").apply { this.writeText("foo") }

		// setup: configure project
		// setup: new project
		val project : Project = ProjectBuilder.builder().build()

		// setup: apply plugin: 'de.h1software.bitbucket.upload'
		project.applyPluginAndAddTask("uploadToBitbucket")

		// exercise: configure task upload properties
		val task : UploadToBitbucket = project.tasks.getByPath("uploadToBitbucket") as UploadToBitbucket
		task.bitbucketProject(Action {
			it.name = "goo/zar"
			it.apiBaseUrl = this.mockBitbucketApiServer.url.toString()
		})
		task.username = "moo"
		task.password = "jar"
		task.console = null
		task.archive(Action {
			it.name = "FOO.TxT"
			it.file = fileToUpload
		})

		// setup
		task.uploadFiles()

		// guard - file now exists
		this.mockBitbucketApiServer.assertFileUploaded("goo/zar", "moo", "jar", "FOO.TxT", fileToUpload.readBytes())

		// exercise - file exists now
		try {
			task.uploadFiles()
			Assert.fail("Could upload same file twice")
		} catch (e : GradleException) {
			Assert.assertTrue("non-informative error: ${e.message}", e.message?.contains("already exists") ?: false)
		}
	}

	@Test
	fun overwritingAResourceIsDoneWhenExplicitlyConfigured() {

		// setup: create project dir
		val projectDir = this.temporaryFolder.newFolder()

		// setup: create files to upload
		val fileToUpload : File = projectDir.resolve("foo.txt").apply { this.writeText("foo") }

		// setup: configure project
		// setup: new project
		val project : Project = ProjectBuilder.builder().build()

		// setup: apply plugin: 'de.h1software.bitbucket.upload'
		project.applyPluginAndAddTask("uploadToBitbucket")

		// setup: configure task upload properties
		val task : UploadToBitbucket = project.tasks.getByPath("uploadToBitbucket") as UploadToBitbucket
		task.bitbucketProject(Action {
			it.name = "goo/zar"
			it.apiBaseUrl = this.mockBitbucketApiServer.url.toString()
		})
		task.username = "moo"
		task.password = "jar"
		task.console = null
		task.archive(Action {
			it.name = "FOO.TxT"
			it.file = fileToUpload
		})

		// setup
		task.overwrite = true

		// setup
		task.uploadFiles()

		// guard - file now exists
		this.mockBitbucketApiServer.assertFileUploaded("goo/zar", "moo", "jar", "FOO.TxT", fileToUpload.readBytes())

		// exercise - file exists now
		task.uploadFiles()

		// check
		this.mockBitbucketApiServer.assertFileUploaded("goo/zar", "moo", "jar", "FOO.TxT", fileToUpload.readBytes())
	}
}