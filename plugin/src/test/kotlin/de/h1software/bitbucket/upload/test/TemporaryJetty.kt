package de.h1software.bitbucket.upload.test

import de.h1software.bitbucket.upload.findBindablePort
import org.eclipse.jetty.server.Handler
import org.eclipse.jetty.server.Server
import org.eclipse.jetty.server.handler.ContextHandlerCollection
import org.eclipse.jetty.server.handler.DefaultHandler
import org.eclipse.jetty.server.handler.HandlerCollection
import org.eclipse.jetty.server.handler.RequestLogHandler
import org.eclipse.jetty.servlet.ServletHolder
import org.eclipse.jetty.webapp.WebAppContext
import org.junit.rules.TemporaryFolder
import java.net.InetAddress
import java.net.InetSocketAddress
import java.net.URL
import javax.servlet.http.HttpServlet

// TODO generalize to socketFactory : () -> InetSocketAddress
class TemporaryJetty(bindAddress : InetAddress = InetAddress.getLoopbackAddress(),
					 portRange : IntRange = 8000..8999,
					 servlets : Iterable<Pair<List<String>, HttpServlet>>) : TemporaryFolder() {

	private val port : Int by lazy { bindAddress.findBindablePort(portRange) }
	private val bindSocketAddress : InetSocketAddress by lazy {
		InetSocketAddress(bindAddress, this.port)
	}
	val baseUrl : URL get() = URL("http://${this.bindSocketAddress.hostName}:${this.bindSocketAddress.port}")

	private val server : Server by lazy {
		super.before()
		val webappContext = WebAppContext().apply {
			this.classLoader = Thread.currentThread().contextClassLoader
			this.isParentLoaderPriority = true
			this.contextPath = "/"
			this.resourceBase = this@TemporaryJetty.newFolder().absolutePath
		}

		servlets.flatMap { (paths : List<String>, servlet : HttpServlet) ->
			paths.map { it to servlet }
		}.forEach { (path : String, servlet : HttpServlet) ->
			webappContext.addServlet(ServletHolder(servlet), path)
		}

		val server = Server(this.bindSocketAddress)

		server.handler = HandlerCollection().apply {
			val contexts = ContextHandlerCollection().apply {
				this.addHandler(webappContext)
			}
			this.handlers = arrayOf<Handler>(contexts, DefaultHandler(), RequestLogHandler())
		}

		server.stopAtShutdown = true
		server
	}

	public override fun before() {
		super.before()

		this.server.start()
	}

	public override fun after() {
		try {
			this.server.stop()
		} catch (e : Exception) {
			e.printStackTrace()
		}
		try {
			super.after()
		} catch (e : Exception) {
			e.printStackTrace()
		}
	}


}
