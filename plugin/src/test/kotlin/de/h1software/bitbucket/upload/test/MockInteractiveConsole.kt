package de.h1software.bitbucket.upload.test

import de.h1software.bitbucket.upload.InteractiveConsole

class MockInteractiveConsole(private val username : String, private val password : String) : InteractiveConsole {

	override fun readUsername(prompt : String) : String  = this.username

	override fun readPassword(prompt : String) : String = this.password
}