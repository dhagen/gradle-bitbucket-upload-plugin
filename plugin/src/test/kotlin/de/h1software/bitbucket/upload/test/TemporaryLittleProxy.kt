package de.h1software.bitbucket.upload.test

import de.h1software.bitbucket.upload.findBindablePort
import de.h1software.bitbucket.upload.isInitialized
import io.netty.handler.codec.http.HttpRequest
import org.junit.Assert
import org.junit.rules.ExternalResource
import org.littleshoot.proxy.HttpFilters
import org.littleshoot.proxy.HttpFiltersSourceAdapter
import org.littleshoot.proxy.HttpProxyServer
import org.littleshoot.proxy.ProxyAuthenticator
import org.littleshoot.proxy.impl.DefaultHttpProxyServer
import java.net.InetAddress
import java.net.InetSocketAddress

class TemporaryLittleProxy(private val requiredCredentials : Pair<String, String>? = null,
						   bindAddress : InetAddress = InetAddress.getLoopbackAddress(),
						   portRange : IntRange = 7000..7999) : ExternalResource() {


	val requiredUsername : String? get() = this.requiredCredentials?.first
	val requiredPassword : String? get() = this.requiredCredentials?.second

	private val requestUris : MutableList<String> = ArrayList()

	val httpProxyServer : HttpProxyServer by lazy {
		DefaultHttpProxyServer
				.bootstrap()
				.withAddress(InetSocketAddress(bindAddress, bindAddress.findBindablePort(portRange)))
				.withAllowLocalOnly(true)
				.apply {
					this@TemporaryLittleProxy.requiredCredentials?.let { requiredCredentails : Pair<String, String> ->
						this.withProxyAuthenticator(object : ProxyAuthenticator {
							override fun authenticate(userName : String?, password : String?) : Boolean
									= requiredCredentails.first == userName && requiredCredentails.second == password

							override fun getRealm() : String = "Foo"

						})
					}
				}.withFiltersSource(object : HttpFiltersSourceAdapter() {
					override fun filterRequest(originalRequest : HttpRequest?) : HttpFilters {
						originalRequest?.uri?.let {
							this@TemporaryLittleProxy.requestUris.add(it)
						}
						return super.filterRequest(originalRequest)
					}
		}).start()
	}

	val port : Int get() = this.httpProxyServer.listenAddress.port
	val host : String get() = this.httpProxyServer.listenAddress.hostName

	override fun after() {
		if (this::httpProxyServer.isInitialized()) {
			this.httpProxyServer.abort()
		}

		super.after()
	}

	fun assertRequestMade(uri : String, message : String = "") : Unit {
		Assert.assertTrue("${message}expected request uri $uri not found in $this requests: ${this.requestUris}", uri in this.requestUris)
	}
	fun assertNoRequestMade() {
		Assert.assertTrue("Request was found in $this requests: ${this.requestUris}", this.requestUris.isEmpty())
	}

	override fun toString() : String = this.httpProxyServer.listenAddress.toString()
}