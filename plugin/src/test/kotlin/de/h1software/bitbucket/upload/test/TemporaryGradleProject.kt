package de.h1software.bitbucket.upload.test

import org.gradle.testkit.runner.GradleRunner
import org.junit.rules.TemporaryFolder
import java.io.File
import java.nio.charset.Charset

class TemporaryGradleProject : TemporaryFolder() {

	val projectDir : File by lazy {
		this.newFolder()
	}

	val buildDotGradle : File get() = this.projectDir.resolve("build.gradle")

	val settingsDotGradle : File get() = this.projectDir.resolve("settings.gradle")

	val gradleDotProperties : File get() = this.projectDir.resolve("gradle.properties")

	fun gradleRunner() : GradleRunner = GradleRunner.create().withPluginClasspath().withProjectDir(this.projectDir).forwardOutput()

	fun buildFile(contents : String, charset : Charset = Charsets.UTF_8) : Unit = this.buildDotGradle.writeText(contents, charset)

	fun projectProperties(contents : String, charset : Charset = Charsets.UTF_8) = this.gradleDotProperties.writeText(contents, charset)

	fun projectSettings(contents : String, charset : Charset = Charsets.UTF_8) = this.settingsDotGradle.writeText(contents, charset)
}