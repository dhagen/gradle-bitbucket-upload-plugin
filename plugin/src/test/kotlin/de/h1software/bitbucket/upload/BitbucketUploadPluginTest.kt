package de.h1software.bitbucket.upload

import de.h1software.bitbucket.upload.test.FakeBitbucketApiServer
import de.h1software.bitbucket.upload.test.TemporaryGradleProject
import de.h1software.bitbucket.upload.test.TemporaryLittleProxy
import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.TaskOutcome
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import java.io.File

class BitbucketUploadPluginTest {

	@get:Rule
	val mockBitbucketApiServer = FakeBitbucketApiServer()

	@get:Rule
	val temporaryGradleProject = TemporaryGradleProject()

	@get:Rule
	val temporaryLittleProxy = TemporaryLittleProxy()

	@get:Rule
	val temporaryLittleProxyWithAuthentication = TemporaryLittleProxy(requiredCredentials = "proxymoo" to "proxyjar")

	@Test
	fun fileCanBeUploaded() {
		// setup: create files to upload
		val filesToUpload : List<File> = listOf(
				this.temporaryGradleProject.projectDir.resolve("foo.txt").apply { this.writeText("foo") },
				this.temporaryGradleProject.projectDir.resolve("bar.txt").apply { this.writeText("bar") }
		)

		// setup: create gradle project configuration
		//language=Groovy
		this.temporaryGradleProject.buildFile("""
			plugins {
				id 'de.h1software.bitbucket.upload'
			}

			task publish(type : de.h1software.bitbucket.upload.UploadToBitbucket) {
				bitbucketProject {
					name = "goo/zar"
					apiBaseUrl = "${this.mockBitbucketApiServer.url}"
				}
				username = "moo"
				password = "jar"

				archive {
					name = "FOO.TxT"
					file = file("${filesToUpload[0].absolutePath}")
				}

				archive {
					name = "BAR.TxT"
					file = file("${filesToUpload[1].absolutePath}")
				}
			}
		""".trimIndent())

		// exercise
		val buildResult : BuildResult = this.temporaryGradleProject.gradleRunner().withArguments("publish").build()

		// check
		Assert.assertEquals(TaskOutcome.SUCCESS, buildResult.task(":publish")?.outcome)
		this.mockBitbucketApiServer.assertFileUploaded("goo/zar", "moo", "jar", "FOO.TxT", filesToUpload[0].readBytes())
		this.mockBitbucketApiServer.assertFileUploaded("goo/zar", "moo", "jar", "BAR.TxT", filesToUpload[1].readBytes())
	}

	@Test
	fun filesCanBeUploadedUsingFullApiUrl() {
		// setup: create files to upload
		val filesToUpload : List<File> = listOf(
				this.temporaryGradleProject.projectDir.resolve("foo.txt").apply { this.writeText("foo") },
				this.temporaryGradleProject.projectDir.resolve("bar.txt").apply { this.writeText("bar") }
		)

		// setup: create gradle project configuration
		//language=Groovy
		this.temporaryGradleProject.buildFile("""
			plugins {
				id 'de.h1software.bitbucket.upload'
			}

			task uploadToBitbucket(type : de.h1software.bitbucket.upload.UploadToBitbucket) {

				apiProjectUrl = "${this.mockBitbucketApiServer.url}/repositories/goo/zar"

				username = "moo"
				password = "jar"

				archive {
					name = "FOO.TxT"
					file = file("${filesToUpload[0].absolutePath}")
				}

				archive {
					name = "BAR.TxT"
					file = file("${filesToUpload[1].absolutePath}")
				}
			}
		""".trimIndent())

		// exercise
		val buildResult : BuildResult = this.temporaryGradleProject.gradleRunner().withArguments("uploadToBitbucket").build()

		// check
		Assert.assertEquals(TaskOutcome.SUCCESS, buildResult.task(":uploadToBitbucket")?.outcome)
		this.mockBitbucketApiServer.assertFileUploaded("goo/zar", "moo", "jar", "FOO.TxT", filesToUpload[0].readBytes())
		this.mockBitbucketApiServer.assertFileUploaded("goo/zar", "moo", "jar", "BAR.TxT", filesToUpload[1].readBytes())
	}

	@Test
	fun httpProxyCanBeUsed() {
		// setup
		this.temporaryGradleProject.projectProperties("""
				systemProp.http.proxyHost=${this.temporaryLittleProxy.host}
				systemProp.http.proxyPort=${this.temporaryLittleProxy.port}
				# See http://bugs.java.com/view_bug.do?bug_id=6737819
				systemProp.http.nonProxyHosts=
			""".trimIndent());

		// setup: create files to upload
		val filesToUpload : List<File> = listOf(
				this.temporaryGradleProject.projectDir.resolve("foo.txt").apply { this.writeText("foo") },
				this.temporaryGradleProject.projectDir.resolve("bar.txt").apply { this.writeText("bar") }
		)

		// setup: create gradle project configuration
		//language=Groovy
		this.temporaryGradleProject.buildFile("""import org.gradle.api.GradleException
			plugins {
				id 'de.h1software.bitbucket.upload'
			}

			task uploadToBitbucket(type : de.h1software.bitbucket.upload.UploadToBitbucket) {
				bitbucketProject {
					name = "goo/zar"
					apiBaseUrl = "${this.mockBitbucketApiServer.url}"
				}
				username = "moo"
				password = "jar"

				archive {
					name = "FOO.TxT"
					file = file("${filesToUpload[0].absolutePath}")
				}

				archive {
					name = "BAR.TxT"
					file = file("${filesToUpload[1].absolutePath}")
				}
			}
		""".trimIndent())
		// exercise
		val buildResult : BuildResult = this.temporaryGradleProject.gradleRunner().withArguments("uploadToBitbucket").build()

		// check
		Assert.assertEquals("build failed with output ${buildResult.output}", TaskOutcome.SUCCESS, buildResult.task(":uploadToBitbucket")?.outcome)

		this.temporaryLittleProxy.assertRequestMade("${this.mockBitbucketApiServer.url}/repositories/goo/zar/downloads", "build output ${buildResult.output};\n")

		this.mockBitbucketApiServer.assertFileUploaded("goo/zar", "moo", "jar", "FOO.TxT", filesToUpload[0].readBytes())
		this.mockBitbucketApiServer.assertFileUploaded("goo/zar", "moo", "jar", "BAR.TxT", filesToUpload[1].readBytes())

	}

	@Test
	fun httpProxyWithAuthenticationCanBeUsed() {
		// setup
		this.temporaryGradleProject.projectProperties("""
				systemProp.http.proxyHost=${this.temporaryLittleProxyWithAuthentication.host}
				systemProp.http.proxyPort=${this.temporaryLittleProxyWithAuthentication.port}
				systemProp.http.proxyUser=${this.temporaryLittleProxyWithAuthentication.requiredUsername}
				systemProp.http.proxyPassword=${this.temporaryLittleProxyWithAuthentication.requiredPassword}
				# See http://bugs.java.com/view_bug.do?bug_id=6737819
				systemProp.http.nonProxyHosts=

				systemProp.org.apache.commons.logging.Log=org.apache.commons.logging.impl.SimpleLog
				systemProp.org.apache.commons.logging.simplelog.showdatetime=true
				systemProp.org.apache.commons.logging.simplelog.log.org.apache.http=DEBUG
				systemProp.org.apache.commons.logging.simplelog.log.org.apache.http.wire=DEBUG
			""".trimIndent());

		// setup: create files to upload
		val filesToUpload : List<File> = listOf(
				this.temporaryGradleProject.projectDir.resolve("foo.txt").apply { this.writeText("foo") },
				this.temporaryGradleProject.projectDir.resolve("bar.txt").apply { this.writeText("bar") }
		)

		// setup: create gradle project configuration
		//language=Groovy
		this.temporaryGradleProject.buildFile("""
			plugins {
				id 'de.h1software.bitbucket.upload'
			}

			task uploadToBitbucket(type : de.h1software.bitbucket.upload.UploadToBitbucket) {
				bitbucketProject {
					name = "goo/zar"
					apiBaseUrl = "${this.mockBitbucketApiServer.url}"
				}
				username = "moo"
				password = "jar"

				archive {
					name = "FOO.TxT"
					file = file("${filesToUpload[0].absolutePath}")
				}

				archive {
					name = "BAR.TxT"
					file = file("${filesToUpload[1].absolutePath}")
				}
			}
		""".trimIndent())

		// exercise
		val buildResult : BuildResult = this.temporaryGradleProject.gradleRunner().withArguments("uploadToBitbucket").build()

		// check
		Assert.assertEquals(TaskOutcome.SUCCESS, buildResult.task(":uploadToBitbucket")?.outcome)

		this.temporaryLittleProxyWithAuthentication.assertRequestMade("${this.mockBitbucketApiServer.url}/repositories/goo/zar/downloads")

		this.mockBitbucketApiServer.assertFileUploaded("goo/zar", "moo", "jar", "FOO.TxT", filesToUpload[0].readBytes())
		this.mockBitbucketApiServer.assertFileUploaded("goo/zar", "moo", "jar", "BAR.TxT", filesToUpload[1].readBytes())

	}

	@Test
	fun httpProxyWithAuthenticationRequiresAuthentication() {
		// setup
		this.temporaryGradleProject.projectProperties("""
				systemProp.http.proxyHost=${this.temporaryLittleProxyWithAuthentication.host}
				systemProp.http.proxyPort=${this.temporaryLittleProxyWithAuthentication.port}
				# See http://bugs.java.com/view_bug.do?bug_id=6737819
				systemProp.http.nonProxyHosts=
			""".trimIndent());

		// setup: create files to upload
		val filesToUpload : List<File> = listOf(
				this.temporaryGradleProject.projectDir.resolve("foo.txt").apply { this.writeText("foo") },
				this.temporaryGradleProject.projectDir.resolve("bar.txt").apply { this.writeText("bar") }
		)

		// setup: create gradle project configuration
		//language=Groovy
		this.temporaryGradleProject.buildFile("""
			plugins {
				id 'de.h1software.bitbucket.upload'
			}

			task uploadToBitbucket(type : de.h1software.bitbucket.upload.UploadToBitbucket) {
				bitbucketProject {
					name = "goo/zar"
					apiBaseUrl = "${this.mockBitbucketApiServer.url}"
				}
				username = "moo"
				password = "jar"

				archive {
					name = "FOO.TxT"
					file = file("${filesToUpload[0].absolutePath}")
				}

				archive {
					name = "BAR.TxT"
					file = file("${filesToUpload[1].absolutePath}")
				}
			}
		""".trimIndent())

		// exercise
		this.temporaryGradleProject.gradleRunner().withArguments("uploadToBitbucket").buildAndFail()

		// implicit check: an exception is quite enough
	}

	@Test
	fun requestToNonProxyHostIsNotRoutedViaProxy() {

		// setup
		this.temporaryGradleProject.projectProperties("""
				systemProp.http.proxyHost=${this.temporaryLittleProxy.host}
				systemProp.http.proxyPort=${this.temporaryLittleProxy.port}
				# See http://bugs.java.com/view_bug.do?bug_id=6737819
				systemProp.http.nonProxyHosts=*.example.com|localhost
			""".trimIndent());

		// setup: create files to upload
		val filesToUpload : List<File> = listOf(
				this.temporaryGradleProject.projectDir.resolve("foo.txt").apply { this.writeText("foo") },
				this.temporaryGradleProject.projectDir.resolve("bar.txt").apply { this.writeText("bar") }
		)

		// setup: create gradle project configuration
		//language=Groovy
		this.temporaryGradleProject.buildFile("""import org.gradle.api.GradleException
			plugins {
				id 'de.h1software.bitbucket.upload'
			}

			task uploadToBitbucket(type : de.h1software.bitbucket.upload.UploadToBitbucket) {
				bitbucketProject {
					name = "goo/zar"
					apiBaseUrl = "${this.mockBitbucketApiServer.url}"
				}
				username = "moo"
				password = "jar"

				archive {
					name = "FOO.TxT"
					file = file("${filesToUpload[0].absolutePath}")
				}

				archive {
					name = "BAR.TxT"
					file = file("${filesToUpload[1].absolutePath}")
				}
			}
		""".trimIndent())
		// exercise
		val buildResult : BuildResult = this.temporaryGradleProject.gradleRunner().withArguments("uploadToBitbucket").build()

		// check
		Assert.assertEquals("build failed with output ${buildResult.output}", TaskOutcome.SUCCESS, buildResult.task(":uploadToBitbucket")?.outcome)

		this.temporaryLittleProxy.assertNoRequestMade()

		this.mockBitbucketApiServer.assertFileUploaded("goo/zar", "moo", "jar", "FOO.TxT", filesToUpload[0].readBytes())
		this.mockBitbucketApiServer.assertFileUploaded("goo/zar", "moo", "jar", "BAR.TxT", filesToUpload[1].readBytes())

	}
}