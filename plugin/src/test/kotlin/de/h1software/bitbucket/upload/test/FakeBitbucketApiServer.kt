package de.h1software.bitbucket.upload.test

import de.h1software.bitbucket.upload.resolve
import org.apache.commons.codec.binary.Base64
import org.apache.http.HttpStatus
import org.eclipse.jetty.server.Request
import org.junit.Assert
import org.junit.rules.TemporaryFolder
import java.io.File
import java.net.URL
import javax.servlet.MultipartConfigElement
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


fun String.splitToPair(separator : String) : Pair<String, String> {
	val indexOfSeparator : Int = this.indexOf(separator)
	if (indexOfSeparator ==-1) {
		throw IllegalArgumentException("separator '$separator' not found in $this")
	} else {
		return this.substring(0, indexOfSeparator) to this.substring(indexOfSeparator + 1)
	}
}

val HttpServletRequest.authorization : String? get() = this.getHeader("Authorization")
val HttpServletRequest.decodedAuthorization : Pair<String, String>? get() = authorization?.let {
	Base64.decodeBase64(it.substring("Basic ".length)).toString(Charsets.UTF_8).splitToPair(":")
}

val HttpServletRequest.referer : String? get() = this.getHeader("Referer")
val HttpServletRequest.refererUrl : URL? get() = this.referer?.let { URL(it) }
val HttpServletRequest.refererRequestUri : String? get() = this.refererUrl?.path

private data class ReceivedRequest(val user: String?, val password : String?, val requestUri : String, val files : List<Pair<String, ByteArray>>) {

	fun contains(expectedProjectName : String, expectedUserName : String, expectedPassword : String, expectedFilename : String, expectedContents : ByteArray) : Boolean {
		return this.requestUri=="/repositories/$expectedProjectName/downloads"
				&& this.user == expectedUserName
				&& this.password == expectedPassword
				&& this.files.any { (name, data) -> name == expectedFilename &&  data.contentEquals(expectedContents) }
	}
}

class FakeBitbucketApiServlet(private val tmpDir : () -> File) : HttpServlet() {

	private val receivedRequests : MutableList<ReceivedRequest> = ArrayList()

	private var requiredCredentials : Pair<String, String>? = null

	fun restrictAccessToCredentials(username : String, password : String) : Unit {
		this.requiredCredentials = username to password
	}

	override fun doPost(request : HttpServletRequest, response : HttpServletResponse) {
		(request as Request).setAttribute(Request.__MULTIPART_CONFIG_ELEMENT, MultipartConfigElement(tmpDir().absolutePath));

		val (username : String?, password : String?) = request.decodedAuthorization ?: (null to null)

		this.requiredCredentials?.let { (requiredUsername, requiredPassword) ->
			if (username!=requiredUsername || password!=requiredPassword) {
				response.sendError(401, "Permission denied")
				return
			}
		}

		val requestUri : String = request.requestURI

		val parts : List<Pair<String, ByteArray>> = request.parts
				.filter { it.name == "files" }
				.map { it.submittedFileName to it.inputStream.readBytes()  }

		this.receivedRequests.add(ReceivedRequest(username, password, requestUri, parts))
	}

	private fun findUpload(requestUri : String) : Pair<String, ByteArray>? {
		val projectUri = requestUri.split("/").dropLast(1).joinToString("/")
		val fileName = requestUri.split("/").last()

		return this.receivedRequests
				.firstOrNull { it.requestUri == projectUri }
				?.files?.find { (name, _) -> name == fileName }
	}

	override fun doHead(request : HttpServletRequest, response : HttpServletResponse) {
		val (cdnRequest : Boolean, actualUri : String) =
				if (request.requestURI.startsWith("/fakecdn")) {
					true to request.requestURI.substring("/fakecdn".length)
				} else {
					false to request.requestURI
				}
		findUpload(actualUri)?.also {
			if (cdnRequest) {
				response.status = HttpStatus.SC_FORBIDDEN
			} else {
				response.sendRedirect("/fakecdn${actualUri}")
			}
		} ?: response.sendError(HttpStatus.SC_NOT_FOUND)
	}

	fun assertFileUploaded(projectName : String, userName : String, password : String, filename : String, contents : ByteArray) : Unit {
		if (!this.receivedRequests.any { it.contains(projectName, userName, password, filename, contents) }) {
			Assert.fail("$this did not receive expected upload $projectName,$userName,$password,$filename,$contents")
		}
	}

	override fun toString() : String = this.receivedRequests.joinToString("\n")

}

class FakeBitbucketApiServer : TemporaryFolder() {

	private val mockApiServlet = FakeBitbucketApiServlet({ newFolder() })
	private val temporaryJetty = TemporaryJetty(servlets = listOf<Pair<List<String>, HttpServlet>>(listOf("/repositories/*", "/fakecdn/*") to this.mockApiServlet))

	override fun before() {
		super.before()
		this.temporaryJetty.before()
	}

	override fun after() {
		this.temporaryJetty.after()
		super.after()
	}

	val url : URL get() = this.temporaryJetty.baseUrl

	fun resolveUrl(path : String) : URL = this.url.resolve(path)

	fun assertFileUploaded(projectName : String, userName : String, password : String, filename : String, contents : ByteArray) : Unit = this.mockApiServlet.assertFileUploaded(projectName, userName, password, filename, contents)
}