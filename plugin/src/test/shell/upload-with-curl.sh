#!/usr/bin/env bash

#
# Usage: upload-with-curl.sh https://api.bitbucket.org/2.0 <user> <user>/<project> <file>
#

API_BASE_URL=$1
BITBUCKET_USER=$2
PROJECT_NAME=$3
FILE_PATH=$4

curl -s -u ${BITBUCKET_USER} -X POST ${API_BASE_URL}/repositories/${PROJECT_NAME}/downloads -F files=@${FILE_PATH} --trace-ascii %